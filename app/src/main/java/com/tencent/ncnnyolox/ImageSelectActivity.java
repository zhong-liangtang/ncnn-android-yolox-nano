// Tencent is pleased to support the open source community by making ncnn available.
//
// Copyright (C) 2021 THL A29 Limited, a Tencent company. All rights reserved.
//
// Licensed under the BSD 3-Clause License (the "License"); you may not use this file except
// in compliance with the License. You may obtain a copy of the License at
//
// https://opensource.org/licenses/BSD-3-Clause
//
// Unless required by applicable law or agreed to in writing, software distributed
// under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

package com.tencent.ncnnyolox;

import android.Manifest;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;


import android.graphics.BitmapFactory;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.graphics.Matrix;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.util.Arrays;

public class ImageSelectActivity extends Activity
{
    private NcnnYolox ncnnyolox = new NcnnYolox();
    private Spinner spinnerModel;
    private Spinner spinnerCPUGPU;
    private int current_model = 0; // yolox-nano
    private int current_cpugpu = 0; // CPU

    private int first_run = 1;
    private static final int SELECT_IMAGE = 1;

    private ImageView imageView;
    private Bitmap bitmap = null;
    private Bitmap yourSelectedImage = null;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_select);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        spinnerModel = (Spinner) findViewById(R.id.spinnerModel);
        spinnerModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id)
            {
                if (position != current_model)
                {
                    current_model = position;
                    reload();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
            }
        });

        spinnerCPUGPU = (Spinner) findViewById(R.id.spinnerCPUGPU);
        spinnerCPUGPU.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id)
            {
                if (position != current_cpugpu)
                {
                    current_cpugpu = position;
                    reload();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
            }
        });

        imageView = (ImageView) findViewById(R.id.imageView);

        Button buttonImage = (Button) findViewById(R.id.buttonImage);
        Button buttonTest = (Button) findViewById(R.id.buttonTest);
        if (first_run == 1) {
            Intent i = new Intent(Intent.ACTION_PICK);
            i.setType("image/*");
            startActivityForResult(i, SELECT_IMAGE);
            first_run = 0;
        }

        handlePermission();

        buttonTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // 安卓拍照
//                Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(it,Activity.DEFAULT_KEYS_DIALER);
                Intent intent = new Intent(ImageSelectActivity.this, TakePhotoActivity.class);
                startActivity(intent);
            }
        });

        buttonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                FileInputStream fs = null;
                try {
                    fs = new FileInputStream(getSDPath() + "/yumi2.jpg");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Bitmap bitmap  = BitmapFactory.decodeStream(fs);
                System.out.println("bitmap:" + bitmap);
                Uri imageUri = Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(),bitmap , "test","test"));
                System.out.println("imageUri:" + imageUri);
                onDetectResult(imageUri);

                // 安卓拍照
//                Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(it,Activity.DEFAULT_KEYS_DIALER);

                // 访问项目的res文件夹下的图片
//                Bitmap bitmap = BitmapFactory.decodeStream(getClass().getResourceAsStream("/res/drawable/yumi.jpg"));
//                System.out.println("bitmap:" + bitmap);
//                Uri imageUri = Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(),bitmap , "test","test"));
//                System.out.println("imageUri:" + imageUri);
//                onDetectResult(imageUri);

//                System.out.println("SD卡路径：" + getSDPath());
//                String picture = getSDPath() + "/IMG_20211004_190011.jpg";
//                System.out.println("path:" + picture);
//                Context context = getApplicationContext();
//                Uri p = getMediaUriFromPath(context, picture);
//                System.out.println("p:" + p);

//                Intent i = new Intent(Intent.ACTION_PICK);
                // 这是正常的访问系统自带的文件管理器。但是setType只支持单个setType一般是以下这种(以只查看图片文件为例):
//                i.setType("image/*");
//                startActivityForResult(i, SELECT_IMAGE);

//                onDetectResult(imageUri);
            }
        });

        Button buttonSwitchCamera = (Button) findViewById(R.id.buttonSwitchCamera);
        buttonSwitchCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(ImageSelectActivity.this, MainActivity.class);
                startActivity(intent);

            }
        });

        reload();
    }

    /**
     * 检查app是否拥有存储权限，如果没有的话，提醒用户开启权限
     */
    public void handlePermission() {
        // 检查是否开启 Manifest.permission.xxx
        // (xxx 为权限，根据自己需求添加）
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Permission has been allowed", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "ask for permission",Toast.LENGTH_SHORT).show();
            // 请求权限
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//            Log.d(TAG, "handlePermission: has aksed");
            System.out.println("handlePermission: has aksed");
        }
    }


    public String getSDPath(){
        File sdDir = null;
        boolean sdCardExist = Environment.getExternalStorageState()
                .equals(android.os.Environment.MEDIA_MOUNTED);//判断sd卡是否存在
        if(sdCardExist)
        {
            sdDir = Environment.getExternalStorageDirectory();//获取跟目录
        }
        return sdDir.toString();
    }

    private void reload()
    {
        boolean ret_init = ncnnyolox.loadModel(getAssets(), current_model, current_cpugpu);
        if (!ret_init)
        {
            Log.e("MainActivity", "ncnnyolox loadModel failed");
        }
    }

    private void showObjects(NcnnYolox.Obj[] objects)
    {
        if (objects == null)
        {
            imageView.setImageBitmap(bitmap);
            return;
        }

        // draw objects on bitmap
        Bitmap rgba = bitmap.copy(Bitmap.Config.ARGB_8888, true);

        final int[] colors = new int[] {
                Color.rgb( 54,  67, 244),
                Color.rgb( 99,  30, 233),
                Color.rgb(176,  39, 156),
                Color.rgb(183,  58, 103),
                Color.rgb(181,  81,  63),
                Color.rgb(243, 150,  33),
                Color.rgb(244, 169,   3),
                Color.rgb(212, 188,   0),
                Color.rgb(136, 150,   0),
                Color.rgb( 80, 175,  76),
                Color.rgb( 74, 195, 139),
                Color.rgb( 57, 220, 205),
                Color.rgb( 59, 235, 255),
                Color.rgb(  7, 193, 255),
                Color.rgb(  0, 152, 255),
                Color.rgb( 34,  87, 255),
                Color.rgb( 72,  85, 121),
                Color.rgb(158, 158, 158),
                Color.rgb(139, 125,  96)
        };

        Canvas canvas = new Canvas(rgba);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(4);

        Paint textbgpaint = new Paint();
        //textbgpaint.setColor(Color.WHITE);
        textbgpaint.setStyle(Paint.Style.FILL);

        Paint textpaint = new Paint();
        //textpaint.setColor(Color.BLACK);
        textpaint.setTextSize(28);
        textpaint.setTextAlign(Paint.Align.LEFT);

        for (int i = 0; i < objects.length; i++)
        {
            paint.setColor(colors[i % 19]);
            textbgpaint.setColor(colors[i % 19]);
            textpaint.setColor(Color.WHITE);

            canvas.drawRect(objects[i].x, objects[i].y, objects[i].x + objects[i].w, objects[i].y + objects[i].h, paint);

            // draw filled text inside image
            {
                String text = objects[i].label + "  " + String.format("%.1f", objects[i].prob * 100) + "%";

                float text_width = textpaint.measureText(text);
                float text_height = - textpaint.ascent() + textpaint.descent();

                float x = objects[i].x;
                float y = objects[i].y - text_height;
                if (y < 0)
                    y = 0;
                if (x + text_width > rgba.getWidth())
                    x = rgba.getWidth() - text_width;

                canvas.drawRect(x, y, x + text_width, y + text_height, textbgpaint);

                canvas.drawText(text, x, y - textpaint.ascent(), textpaint);
            }
        }

        imageView.setImageBitmap(rgba);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            if(selectedImage != null) {
                System.out.println("Intent data:" + data);
                System.out.println("data.getData():" + data.getData());
                System.out.println("selectedImage:" + selectedImage);
                try
                {
                    if (requestCode == SELECT_IMAGE) {
                        bitmap = decodeUri(selectedImage);

                        yourSelectedImage = bitmap.copy(Bitmap.Config.ARGB_8888, true);

                        imageView.setImageBitmap(bitmap);

                        if (yourSelectedImage == null)
                            return;

                        NcnnYolox.Obj[] objects = ncnnyolox.Detect(yourSelectedImage);

                        showObjects(objects);
                    }
                }
                catch (FileNotFoundException e)
                {
                    Log.e("MainActivity", "FileNotFoundException");
                    return;
                }
            } else {
                System.out.println("hhhhhhhhhhhh");
                Bundle bundle = data.getExtras();
                bitmap = (Bitmap) bundle.get("data");
                System.out.println("---bitmap---" + bitmap);
                selectedImage = Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(),bitmap , "test","test"));
                System.out.println("---selectedImage---" + selectedImage);
                try
                {
                    Bitmap bitmap = decodeUri(selectedImage);

                    yourSelectedImage = bitmap.copy(Bitmap.Config.ARGB_8888, true);
                    System.out.println("---bitmap---" + bitmap);
                    imageView.setImageBitmap(bitmap);

                    if (yourSelectedImage == null)
                        return;

                    NcnnYolox.Obj[] objects = ncnnyolox.Detect(yourSelectedImage);
                    System.out.println("---objects---" + Arrays.toString(objects));

                    showObjects(objects);
                }
                catch (FileNotFoundException e)
                {
                    Log.e("MainActivity", "FileNotFoundException");
                    return;
                }
            }

        }
    }

    private void onDetectResult(Uri imageUri) {

        try
        {
            if (imageUri != null) {
                bitmap = decodeUri(imageUri);

                yourSelectedImage = bitmap.copy(Bitmap.Config.ARGB_8888, true);

                imageView.setImageBitmap(bitmap);

                if (yourSelectedImage == null)
                    return;

                NcnnYolox.Obj[] objects = ncnnyolox.Detect(yourSelectedImage);

                showObjects(objects);
            }
        }
        catch (FileNotFoundException e)
        {
            Log.e("MainActivity", "FileNotFoundException");
            return;
        }
    }

    /***
     * 将指定路径的图片转uri
     * @param context
     * @param path ，指定图片(或文件)的路径
     * @return
     */
    public static Uri getMediaUriFromPath(Context context, String path) {
        Uri mediaUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        Cursor cursor = context.getContentResolver().query(mediaUri,
                null,
                MediaStore.Images.Media.DISPLAY_NAME + "= ?",
                new String[] {path.substring(path.lastIndexOf("/") + 1)},
                null);

        Uri uri = null;
        if(cursor.moveToFirst()) {
            uri = ContentUris.withAppendedId(mediaUri,
                    cursor.getLong(cursor.getColumnIndex(MediaStore.Images.Media._ID)));
        }
        cursor.close();
        return uri;
    }


    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException
    {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 640;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);

        // Rotate according to EXIF
        int rotate = 0;
        try
        {
            ExifInterface exif = new ExifInterface(getContentResolver().openInputStream(selectedImage));
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        }
        catch (IOException e)
        {
            Log.e("MainActivity", "ExifInterface IOException");
        }

        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

}
