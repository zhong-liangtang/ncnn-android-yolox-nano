package com.tencent.ncnnyolox;

import android.Manifest;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TakePhotoActivity extends Activity {

    private NcnnYolox ncnnyolox = new NcnnYolox();
    private Spinner spinnerModel;
    private Spinner spinnerCPUGPU;
    private int current_model = 0; // yolox-nano
    private int current_cpugpu = 0; // CPU

    private int first_run = 1;
    private static final int SELECT_IMAGE = 1;

    private ImageView imageView;
    private Bitmap bitmap = null;
    private Bitmap yourSelectedImage = null;

    //定义一个保存图片的File变量
    private File currentImageFile = null;

    public static final MediaType mediaType = MediaType.parse("application/json;charset=utf-8");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_photo);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        spinnerModel = (Spinner) findViewById(R.id.spinnerModel);
        spinnerModel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id)
            {
                if (position != current_model)
                {
                    current_model = position;
                    reload();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
            }
        });

        spinnerCPUGPU = (Spinner) findViewById(R.id.spinnerCPUGPU);
        spinnerCPUGPU.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id)
            {
                if (position != current_cpugpu)
                {
                    current_cpugpu = position;
                    reload();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0)
            {
            }
        });

        imageView = (ImageView) findViewById(R.id.imageView);

        Button buttonImage = (Button) findViewById(R.id.buttonImage);

        Button buttonTest = (Button) findViewById(R.id.buttonTest);


        handlePermission();

        buttonTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                File dir = new File(Environment.getExternalStorageDirectory(),"pictures");
                if(dir.exists()){
                    dir.mkdirs();
                }
                currentImageFile = new File(dir,System.currentTimeMillis() + ".jpg");
                if(!currentImageFile.exists()){
                    try {
                        currentImageFile.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                it.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(currentImageFile));
                startActivityForResult(it, Activity.DEFAULT_KEYS_DIALER);
            }
        });

        buttonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                Intent i = new Intent(Intent.ACTION_PICK);
                i.setType("image/*");
                startActivityForResult(i, SELECT_IMAGE);
                first_run = 0;


                // 安卓拍照
//                Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(it,Activity.DEFAULT_KEYS_DIALER);

                // 访问项目的res文件夹下的图片
//                Bitmap bitmap = BitmapFactory.decodeStream(getClass().getResourceAsStream("/res/drawable/yumi.jpg"));
//                System.out.println("bitmap:" + bitmap);
//                Uri imageUri = Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(),bitmap , "test","test"));
//                System.out.println("imageUri:" + imageUri);
//                onDetectResult(imageUri);

//                System.out.println("SD卡路径：" + getSDPath());
//                String picture = getSDPath() + "/IMG_20211004_190011.jpg";
//                System.out.println("path:" + picture);
//                Context context = getApplicationContext();
//                Uri p = getMediaUriFromPath(context, picture);
//                System.out.println("p:" + p);

//                Intent i = new Intent(Intent.ACTION_PICK);
                // 这是正常的访问系统自带的文件管理器。但是setType只支持单个setType一般是以下这种(以只查看图片文件为例):
//                i.setType("image/*");
//                startActivityForResult(i, SELECT_IMAGE);

//                onDetectResult(imageUri);
            }
        });

        Button buttonSwitchCamera = (Button) findViewById(R.id.buttonSwitchCamera);
        buttonSwitchCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(TakePhotoActivity.this, AutoTakePhoto.class);
                startActivity(intent);

            }
        });

        reload();

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
//        getDemo();
//        postDemo();
    }

    public void postDemo() {

        /**
         * OkHttp 客户端
         * 注意 : 该类型对象较大, 尽量在应用中创建较少的该类型对象
         * 推荐使用单例
         */
        final OkHttpClient mOkHttpClient;

        // 初始化操作
        mOkHttpClient = new OkHttpClient();

        List<Map> list = new ArrayList<>();

        //{
        //    "mId": "3",
        //    "cId": "4",
        //    "moNum": "1"
        //},
        for (int i = 0; i <= 2; i++) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("mId", String.valueOf(i + 3));
            map.put("cId", "4");
            map.put("moNum", "1");
            list.add(map);
        }

        for (int i = 0; i < list.size(); i++) {
            System.out.println("---list---" + list.get(i).get("mId"));
        }

        RequestBody requestBody = RequestBody.create(mediaType, JSON.toJSONString(list));

//         Request 中封装了请求相关信息
        final Request request = new Request.Builder()
//                .url("http://10.0.2.2:8081/hello")   // 设置请求地址
                .url("http://192.168.175.197:8081/Android/order/list")   // 设置请求地址
                .post(requestBody)                          // 使用 Get 方法
                .build();
//
//        // 同步 Post 请求
        new Thread(new Runnable() {
            @Override
            public void run() {
                Response response = null;
                try {
                    response = mOkHttpClient.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String result = null;
                try {
                    result = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println("---result---" + result);
            }
        }).start();
    }

    public void getDemo() {

        /**
         * OkHttp 客户端
         * 注意 : 该类型对象较大, 尽量在应用中创建较少的该类型对象
         * 推荐使用单例
         */
        final OkHttpClient mOkHttpClient;

        // 初始化操作
        mOkHttpClient = new OkHttpClient();

        // Request 中封装了请求相关信息
        final Request request = new Request.Builder()
//                .url("http://10.0.2.2:8081/hello")   // 设置请求地址
                .url("http://192.168.175.197:8081/Android/hello")   // 设置请求地址
                .get()                          // 使用 Get 方法
                .build();

        // 同步 Get 请求
        new Thread(new Runnable() {
            @Override
            public void run() {
                Response response = null;
                try {
                    response = mOkHttpClient.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String result = null;
                try {
                    result = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println("---result---" + result);
            }
        }).start();



//            // 1. 得到访问地址的URL
//            URL url = new URL("http://localhost:8081/hello");
//            // 2. 得到网络访问对象java.net.HttpURLConnection
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            /* 3. 设置请求参数（过期时间，输入、输出流、访问方式），以流的形式进行连接 */
//            // 设置是否向HttpURLConnection输出
//            connection.setDoOutput(false);
//            // 设置是否从HttpUrlConnection读入
//            connection.setDoInput(true);
//            // 设置请求方式
//            connection.setRequestMethod("GET");
//            // 设置是否使用缓存
//            connection.setUseCaches(true);
//            // 设置此 HttpURLConnection 实例是否应该自动执行 HTTP 重定向
//            connection.setInstanceFollowRedirects(true);
//            // 设置超时时间
//            connection.setConnectTimeout(3000);
//            // 连接
//            connection.connect();
//            // 4. 得到响应状态码的返回值 responseCode
//            int code = connection.getResponseCode();
//            // 5. 如果返回值正常，数据在网络中是以流的形式得到服务端返回的数据
//            String msg = "";
//            // 从流中读取响应信息
//            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//            String line = null;
//            while ((line = reader.readLine()) != null) { // 循环从流中读取
//                msg += line + "\n";
//            }
//            reader.close(); // 关闭流
//            if (code == 200) { // 正常响应
//                // 从流中读取响应信息
//                BufferedReader reader1 = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//                String line1 = null;
//                while ((line = reader1.readLine()) != null) { // 循环从流中读取
//                    msg += line + "\n";
//                }
//                reader.close(); // 关闭流
//            }
//            // 6. 断开连接，释放资源
//            connection.disconnect();
//
//            // 显示响应结果
//            System.out.println(msg);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

    /**
     * 检查app是否拥有存储权限，如果没有的话，提醒用户开启权限
     */
    public void handlePermission() {
        // 检查是否开启 Manifest.permission.xxx
        // (xxx 为权限，根据自己需求添加）
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Permission has been allowed", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "ask for permission",Toast.LENGTH_SHORT).show();
            // 请求权限
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//            Log.d(TAG, "handlePermission: has aksed");
            System.out.println("handlePermission: has aksed");
        }
    }


//    public String getSDPath(){
//        File sdDir = null;
//        boolean sdCardExist = Environment.getExternalStorageState()
//                .equals(android.os.Environment.MEDIA_MOUNTED);//判断sd卡是否存在
//        if(sdCardExist)
//        {
//            sdDir = Environment.getExternalStorageDirectory();//获取跟目录
//        }
//        return sdDir.toString();
//    }

    private void reload()
    {
        boolean ret_init = ncnnyolox.loadModel(getAssets(), current_model, current_cpugpu);
        if (!ret_init)
        {
            Log.e("MainActivity", "ncnnyolox loadModel failed");
        }
    }

    private void showObjects(NcnnYolox.Obj[] objects)
    {
        if (objects == null)
        {
            imageView.setImageBitmap(bitmap);
            return;
        }

        // draw objects on bitmap
        Bitmap rgba = bitmap.copy(Bitmap.Config.ARGB_8888, true);

        final int[] colors = new int[] {
                Color.rgb( 54,  67, 244),
                Color.rgb( 99,  30, 233),
                Color.rgb(176,  39, 156),
                Color.rgb(183,  58, 103),
                Color.rgb(181,  81,  63),
                Color.rgb(243, 150,  33),
                Color.rgb(244, 169,   3),
                Color.rgb(212, 188,   0),
                Color.rgb(136, 150,   0),
                Color.rgb( 80, 175,  76),
                Color.rgb( 74, 195, 139),
                Color.rgb( 57, 220, 205),
                Color.rgb( 59, 235, 255),
                Color.rgb(  7, 193, 255),
                Color.rgb(  0, 152, 255),
                Color.rgb( 34,  87, 255),
                Color.rgb( 72,  85, 121),
                Color.rgb(158, 158, 158),
                Color.rgb(139, 125,  96)
        };

        Canvas canvas = new Canvas(rgba);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(4);

        Paint textbgpaint = new Paint();
        //textbgpaint.setColor(Color.WHITE);
        textbgpaint.setStyle(Paint.Style.FILL);

        Paint textpaint = new Paint();
        //textpaint.setColor(Color.BLACK);
        textpaint.setTextSize(28);
        textpaint.setTextAlign(Paint.Align.LEFT);

        for (int i = 0; i < objects.length; i++)
        {
            paint.setColor(colors[i % 19]);
            textbgpaint.setColor(colors[i % 19]);
            textpaint.setColor(Color.WHITE);

            canvas.drawRect(objects[i].x, objects[i].y, objects[i].x + objects[i].w, objects[i].y + objects[i].h, paint);

            // draw filled text inside image
            {
                String text = objects[i].label + "  " + String.format("%.1f", objects[i].prob * 100) + "%";

                float text_width = textpaint.measureText(text);
                float text_height = - textpaint.ascent() + textpaint.descent();

                float x = objects[i].x;
                float y = objects[i].y - text_height;
                if (y < 0)
                    y = 0;
                if (x + text_width > rgba.getWidth())
                    x = rgba.getWidth() - text_width;

                canvas.drawRect(x, y, x + text_width, y + text_height, textbgpaint);

                canvas.drawText(text, x, y - textpaint.ascent(), textpaint);
            }
        }

        imageView.setImageBitmap(rgba);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        System.out.println("---data---" + data);
        System.out.println("---currentImageFile---" + currentImageFile);

        if (resultCode == RESULT_OK && null != currentImageFile && requestCode == Activity.DEFAULT_KEYS_DIALER) {
            Uri takePhotoImg = Uri.fromFile(currentImageFile);
            System.out.println("---takePhotoImg---" + takePhotoImg);
            try {
                bitmap = decodeUri(takePhotoImg);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            System.out.println("---bitmap---" + bitmap);
            yourSelectedImage = bitmap.copy(Bitmap.Config.ARGB_8888, true);

            imageView.setImageBitmap(bitmap);

            if (yourSelectedImage == null)
                return;

            NcnnYolox.Obj[] objects = ncnnyolox.Detect(yourSelectedImage);

            showObjects(objects);
//            Uri takePhotoImg = Uri.fromFile(currentImageFile);
//            System.out.println("takePhotoImg---");
//            onDetectResult(takePhotoImg);
        } else {
            Uri selectedImage = data.getData();
            if(selectedImage != null) {
                System.out.println("Intent data:" + data);
                System.out.println("data.getData():" + data.getData());
                System.out.println("selectedImage:" + selectedImage);
                try
                {
                    if (requestCode == SELECT_IMAGE) {
                        bitmap = decodeUri(selectedImage);

                        yourSelectedImage = bitmap.copy(Bitmap.Config.ARGB_8888, true);

                        imageView.setImageBitmap(bitmap);

                        if (yourSelectedImage == null)
                            return;

                        NcnnYolox.Obj[] objects = ncnnyolox.Detect(yourSelectedImage);

                        showObjects(objects);
                    }
                }
                catch (FileNotFoundException e)
                {
                    Log.e("MainActivity", "FileNotFoundException");
                    return;
                }
            }
        }
    }

    private void onDetectResult(Uri imageUri) {

        try
        {
            if (imageUri != null) {
                bitmap = decodeUri(imageUri);

                yourSelectedImage = bitmap.copy(Bitmap.Config.ARGB_8888, true);

                imageView.setImageBitmap(bitmap);

                if (yourSelectedImage == null)
                    return;

                NcnnYolox.Obj[] objects = ncnnyolox.Detect(yourSelectedImage);

                showObjects(objects);
            }
        }
        catch (FileNotFoundException e)
        {
            Log.e("MainActivity", "FileNotFoundException");
            return;
        }
    }

    /***
     * 将指定路径的图片转uri
     * @param context
     * @param path ，指定图片(或文件)的路径
     * @return
     */
    public static Uri getMediaUriFromPath(Context context, String path) {
        Uri mediaUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        Cursor cursor = context.getContentResolver().query(mediaUri,
                null,
                MediaStore.Images.Media.DISPLAY_NAME + "= ?",
                new String[] {path.substring(path.lastIndexOf("/") + 1)},
                null);

        Uri uri = null;
        if(cursor.moveToFirst()) {
            uri = ContentUris.withAppendedId(mediaUri,
                    cursor.getLong(cursor.getColumnIndex(MediaStore.Images.Media._ID)));
        }
        cursor.close();
        return uri;
    }


    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException
    {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 640;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);

        // Rotate according to EXIF
        int rotate = 0;
        try
        {
            ExifInterface exif = new ExifInterface(getContentResolver().openInputStream(selectedImage));
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        }
        catch (IOException e)
        {
            Log.e("MainActivity", "ExifInterface IOException");
        }

        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

}