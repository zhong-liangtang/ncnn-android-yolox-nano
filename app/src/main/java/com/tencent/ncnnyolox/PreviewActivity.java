package com.tencent.ncnnyolox;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PreviewActivity extends Activity {

    private NcnnYolox ncnnyolox = new NcnnYolox();
    private Spinner spinnerModel;
    private Spinner spinnerCPUGPU;
    private int current_model = 0; // yolox-nano
    private int current_cpugpu = 0; // CPU

//    private HashMap<String, String> menuMap;

    private int first_run = 1;
    private static final int SELECT_IMAGE = 1;
    private HashMap<String, String> menuMap = new HashMap<>();
    private List<Map> list = new ArrayList<>();

    private ImageView imageView;
    private Button btn_take;
    private TextView textview1;
    private TextView textview2;
    private TextView textview3;
    private Button mbtn;
    private Bitmap bitmap = null;
    private Bitmap yourSelectedImage = null;
    public static final MediaType mediaType = MediaType.parse("application/json;charset=utf-8");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        imageView = new ImageView(this);
        setContentView(R.layout.activity_preview);
        String path = getIntent().getStringExtra("path");
        imageView = (ImageView) findViewById(R.id.imageView);
        btn_take = (Button) findViewById(R.id.btn_take_photo);
        textview1 = (TextView) findViewById(R.id.dish1);
        textview2 = (TextView) findViewById(R.id.dish2);
        textview3 = (TextView) findViewById(R.id.dish3);
        mbtn = (Button) findViewById(R.id.btn_confirmOrder);
        btn_take.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(PreviewActivity.this, AutoTakePhoto.class);
                startActivity(intent);
            }
        });
        final Context that = this;
        mbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
//                Intent intent = new Intent(PreviewActivity.this, AutoTakePhoto.class);
//                startActivity(intent);
                /**
                 * OkHttp 客户端
                 * 注意 : 该类型对象较大, 尽量在应用中创建较少的该类型对象
                 * 推荐使用单例
                 */
                if (list != null) {
                    final OkHttpClient mOkHttpClient;

                    // 初始化操作
                    mOkHttpClient = new OkHttpClient();

                    RequestBody requestBody = RequestBody.create(mediaType, JSON.toJSONString(list));

//         Request 中封装了请求相关信息
                    final Request request = new Request.Builder()
//                .url("http://10.0.2.2:8081/hello")   // 设置请求地址
                            .url("http://10.0.2.2:8081/Android/order/list")   // 设置请求地址
                            .post(requestBody)                          // 使用 Get 方法
                            .build();
//
//        // 同步 Post 请求
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Response response = null;
                            try {
                                response = mOkHttpClient.newCall(request).execute();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            String result = null;
                            try {
                                result = response.body().string();

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            System.out.println("---result---" + result);
                        }
                    }).start();

                    AlertDialog.Builder builder = new AlertDialog.Builder(that);
                    builder = new AlertDialog.Builder(that);

                    builder.setTitle("下单提示")
                            .setItems(new String[]{"下单成功"}, null)
                            .setNegativeButton("确定", null);

                    builder.create().show();


                }
            }
        });
//        getImgView(path);
        if(path != null) {
            imageView.setImageURI(Uri.fromFile(new File(path)));
            getImgView(path);
        }
//        setContentView(imageView);
        // "白菜",  //
        // "包菜", //
        // "菜心", //
        // "豆腐", //
        // "豆角炒肉",
        // "粉丝", //
        // "韭菜炒蛋", //
        // "螺旋炒粉", //
        // "牛肉青椒 ", //
        // "糖醋排骨", //
        // "土豆", //
        // "西红柿", //
        // "西兰花", //
        // "玉米", //
        // "孜然肉" //
//        menuMap.put("玉米", "1"); //
//        menuMap.put("韭菜", "2"); //
//        menuMap.put("粉丝", "3"); //
//        menuMap.put("孜然肉", "4"); //
//        menuMap.put("西兰花", "5"); //
//        menuMap.put("西红柿", "6"); //
//        menuMap.put("土豆", "7"); //
//        menuMap.put("糖醋排骨", "8"); //
//        menuMap.put("包菜", "9"); //
//        menuMap.put("牛肉青椒", "10"); //
//        menuMap.put("螺旋炒粉", "11");
//        menuMap.put("韭菜炒蛋", "12");
//        menuMap.put("白菜", "13");
//        menuMap.put("菜心", "14");
//        menuMap.put("豆腐", "15");
//        menuMap.put("豆角炒肉", "16");
    }

    private void toast() {

    }

    public void getImgView(String path) {
        if(path != null){
            imageView.setImageURI(Uri.fromFile(new File(path)));
            Uri takePhotoImg = Uri.fromFile(new File(path));
            System.out.println("---takePhotoImg---" + takePhotoImg);
            try {
                bitmap = decodeUri(takePhotoImg);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            System.out.println("---bitmap---" + bitmap);
            yourSelectedImage = bitmap.copy(Bitmap.Config.ARGB_8888, true);

            imageView.setImageBitmap(bitmap);

            if (yourSelectedImage == null)
                return;

            NcnnYolox.Obj[] objects = ncnnyolox.Detect(yourSelectedImage);

            showObjects(objects);
        }
    }


    /**
     * 检查app是否拥有存储权限，如果没有的话，提醒用户开启权限
     */
    public void handlePermission() {
        // 检查是否开启 Manifest.permission.xxx
        // (xxx 为权限，根据自己需求添加）
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Permission has been allowed", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "ask for permission",Toast.LENGTH_SHORT).show();
            // 请求权限
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
//            Log.d(TAG, "handlePermission: has aksed");
            System.out.println("handlePermission: has aksed");
        }
    }


    public String getSDPath(){
        File sdDir = null;
        boolean sdCardExist = Environment.getExternalStorageState()
                .equals(android.os.Environment.MEDIA_MOUNTED);//判断sd卡是否存在
        if(sdCardExist)
        {
            sdDir = Environment.getExternalStorageDirectory();//获取跟目录
        }
        return sdDir.toString();
    }

    private void reload()
    {
        boolean ret_init = ncnnyolox.loadModel(getAssets(), current_model, current_cpugpu);
        if (!ret_init)
        {
            Log.e("MainActivity", "ncnnyolox loadModel failed");
        }
    }

    private void showObjects(NcnnYolox.Obj[] objects)
    {


        menuMap.put("玉米", "1"); //
        menuMap.put("韭菜", "2"); //
        menuMap.put("粉丝", "3"); //
        menuMap.put("孜然肉", "4"); //
        menuMap.put("西兰花", "5"); //
        menuMap.put("西红柿", "6"); //
        menuMap.put("土豆", "7"); //
        menuMap.put("糖醋排骨", "8"); //
        menuMap.put("包菜", "9"); //
        menuMap.put("牛肉青椒", "10"); //
        menuMap.put("螺旋炒粉", "11");
        menuMap.put("韭菜炒蛋", "12");
        menuMap.put("白菜", "13");
        menuMap.put("菜心", "14");
        menuMap.put("豆腐", "15");
        menuMap.put("豆角炒肉", "16");
        if (objects == null)
        {
            imageView.setImageBitmap(bitmap);
            return;
        }

        // draw objects on bitmap
        Bitmap rgba = bitmap.copy(Bitmap.Config.ARGB_8888, true);

        final int[] colors = new int[] {
                Color.rgb( 54,  67, 244),
                Color.rgb( 99,  30, 233),
                Color.rgb(176,  39, 156),
                Color.rgb(183,  58, 103),
                Color.rgb(181,  81,  63),
                Color.rgb(243, 150,  33),
                Color.rgb(244, 169,   3),
                Color.rgb(212, 188,   0),
                Color.rgb(136, 150,   0),
                Color.rgb( 80, 175,  76),
                Color.rgb( 74, 195, 139),
                Color.rgb( 57, 220, 205),
                Color.rgb( 59, 235, 255),
                Color.rgb(  7, 193, 255),
                Color.rgb(  0, 152, 255),
                Color.rgb( 34,  87, 255),
                Color.rgb( 72,  85, 121),
                Color.rgb(158, 158, 158),
                Color.rgb(139, 125,  96)
        };

        Canvas canvas = new Canvas(rgba);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(4);

        Paint textbgpaint = new Paint();
        //textbgpaint.setColor(Color.WHITE);
        textbgpaint.setStyle(Paint.Style.FILL);

        Paint textpaint = new Paint();
        //textpaint.setColor(Color.BLACK);
        textpaint.setTextSize(28);
        textpaint.setTextAlign(Paint.Align.LEFT);

        for (int i = 0; i < objects.length; i++)
        {
            paint.setColor(colors[i % 19]);
            textbgpaint.setColor(colors[i % 19]);
            textpaint.setColor(Color.WHITE);

            canvas.drawRect(objects[i].x, objects[i].y, objects[i].x + objects[i].w, objects[i].y + objects[i].h, paint);

            // draw filled text inside image
            {
                String text = objects[i].label + "  " + String.format("%.1f", objects[i].prob * 100) + "%";
                System.out.println("菜品信息" + i + "为：" + objects[i].label);
                int j = i + 1;
                if (0 == i) {
                    String menu = objects[i].label;
                    textview1.setText("菜品信息" + j + "为：" + objects[i].label);
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("mId", menuMap.get(menu));
                    map.put("cId", "4");
                    map.put("moNum", "1");
                    list.add(map);
                    System.out.println("===" + menuMap.get(menu) + "===");
                }
                if (1 == i) {
                    String menu = objects[i].label;
                    textview2.setText("菜品信息" + j + "为：" + objects[i].label);
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("mId", menuMap.get(menu));
                    map.put("cId", "4");
                    map.put("moNum", "1");
                    list.add(map);
                    System.out.println("===" + menuMap.get(menu) + "===");
                }
                if (2 == i) {
                    String menu = objects[i].label;
                    textview3.setText("菜品信息" + j + "为：" + objects[i].label);
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("mId", menuMap.get(menu));
                    map.put("cId", "4");
                    map.put("moNum", "1");
                    list.add(map);
                    System.out.println("===" + menuMap.get(menu) + "===");
                }
                float text_width = textpaint.measureText(text);
                float text_height = - textpaint.ascent() + textpaint.descent();

                float x = objects[i].x;
                float y = objects[i].y - text_height;
                if (y < 0)
                    y = 0;
                if (x + text_width > rgba.getWidth())
                    x = rgba.getWidth() - text_width;

                canvas.drawRect(x, y, x + text_width, y + text_height, textbgpaint);

                canvas.drawText(text, x, y - textpaint.ascent(), textpaint);
            }
        }
        for (int i = 0; i < list.size(); i++) {
            System.out.println("---list---mId---" + list.get(i).get("mId"));
            System.out.println("---list---cId---" + list.get(i).get("cId"));
            System.out.println("---list---moNum---" + list.get(i).get("moNum"));
        }
        /**
         * OkHttp 客户端
         * 注意 : 该类型对象较大, 尽量在应用中创建较少的该类型对象
         * 推荐使用单例
         */
        final OkHttpClient mOkHttpClient;

        // 初始化操作
        mOkHttpClient = new OkHttpClient();

        RequestBody requestBody = RequestBody.create(mediaType, JSON.toJSONString(list));

//         Request 中封装了请求相关信息
        final Request request = new Request.Builder()
                .url("http://10.0.2.2:8081/hello")   // 设置请求地址
//                .url("http://192.168.175.197:8081/Android/order/list")   // 设置请求地址
//                .url("http://10.0.2.2:8081/Android/order/list")   // 设置请求地址
                .post(requestBody)                          // 使用 Get 方法
                .build();
//
//        // 同步 Post 请求
        new Thread(new Runnable() {
            @Override
            public void run() {
                Response response = null;
                try {
                    response = mOkHttpClient.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String result = null;
                try {
                    result = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println("---result---" + result);
            }
        }).start();

        imageView.setImageBitmap(rgba);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            if(selectedImage != null) {
                System.out.println("Intent data:" + data);
                System.out.println("data.getData():" + data.getData());
                System.out.println("selectedImage:" + selectedImage);
                try
                {
                    if (requestCode == SELECT_IMAGE) {
                        bitmap = decodeUri(selectedImage);

                        yourSelectedImage = bitmap.copy(Bitmap.Config.ARGB_8888, true);

                        imageView.setImageBitmap(bitmap);

                        if (yourSelectedImage == null)
                            return;

                        NcnnYolox.Obj[] objects = ncnnyolox.Detect(yourSelectedImage);

                        showObjects(objects);
                    }
                }
                catch (FileNotFoundException e)
                {
                    Log.e("MainActivity", "FileNotFoundException");
                    return;
                }
            } else {
                System.out.println("hhhhhhhhhhhh");
                Bundle bundle = data.getExtras();
                bitmap = (Bitmap) bundle.get("data");
                System.out.println("---bitmap---" + bitmap);
                selectedImage = Uri.parse(MediaStore.Images.Media.insertImage(getContentResolver(),bitmap , "test","test"));
                System.out.println("---selectedImage---" + selectedImage);
                try
                {
                    Bitmap bitmap = decodeUri(selectedImage);

                    yourSelectedImage = bitmap.copy(Bitmap.Config.ARGB_8888, true);
                    System.out.println("---bitmap---" + bitmap);
                    imageView.setImageBitmap(bitmap);

                    if (yourSelectedImage == null)
                        return;

                    NcnnYolox.Obj[] objects = ncnnyolox.Detect(yourSelectedImage);
                    System.out.println("---objects---" + Arrays.toString(objects));

                    showObjects(objects);
                }
                catch (FileNotFoundException e)
                {
                    Log.e("MainActivity", "FileNotFoundException");
                    return;
                }
            }

        }
    }

    private void onDetectResult(Uri imageUri) {

        try
        {
            if (imageUri != null) {
                bitmap = decodeUri(imageUri);

                yourSelectedImage = bitmap.copy(Bitmap.Config.ARGB_8888, true);

                imageView.setImageBitmap(bitmap);

                if (yourSelectedImage == null)
                    return;

                NcnnYolox.Obj[] objects = ncnnyolox.Detect(yourSelectedImage);

                showObjects(objects);
            }
        }
        catch (FileNotFoundException e)
        {
            Log.e("MainActivity", "FileNotFoundException");
            return;
        }
    }

    /***
     * 将指定路径的图片转uri
     * @param context
     * @param path ，指定图片(或文件)的路径
     * @return
     */
    public static Uri getMediaUriFromPath(Context context, String path) {
        Uri mediaUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        Cursor cursor = context.getContentResolver().query(mediaUri,
                null,
                MediaStore.Images.Media.DISPLAY_NAME + "= ?",
                new String[] {path.substring(path.lastIndexOf("/") + 1)},
                null);

        Uri uri = null;
        if(cursor.moveToFirst()) {
            uri = ContentUris.withAppendedId(mediaUri,
                    cursor.getLong(cursor.getColumnIndex(MediaStore.Images.Media._ID)));
        }
        cursor.close();
        return uri;
    }


    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException
    {
        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 640;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);

        // Rotate according to EXIF
        int rotate = 0;
        try
        {
            ExifInterface exif = new ExifInterface(getContentResolver().openInputStream(selectedImage));
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        }
        catch (IOException e)
        {
            Log.e("MainActivity", "ExifInterface IOException");
        }

        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

}